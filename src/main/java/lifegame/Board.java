package lifegame;

/**
 * The grid in which the game will take place
 */
public class Board {

    private final CellState[][] grid;

    public Board(int rows, int columns) {
        grid = new CellState[rows][columns];
    }

    /**
     * The get method returns state of cell at given position
     *
     * @param row    - row of the grid
     * @param column - column of the grid
     * @return state of cell stored at given position
     */
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    public void set(int row, int column, CellState value) {
        grid[row][column] = value;
    }
}