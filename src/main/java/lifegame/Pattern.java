package lifegame;

import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.List;

public enum Pattern {

    GLIDER(List.of(new ImmutablePair<>(4, 1), new ImmutablePair<>(5, 2),
            new ImmutablePair<>(5, 3), new ImmutablePair<>(4, 3),
            new ImmutablePair<>(3, 3)));

    Pattern(List<ImmutablePair<Integer, Integer>> positions) {
        this.positions = positions;
    }

    private final List<ImmutablePair<Integer, Integer>> positions;

    public List<ImmutablePair<Integer, Integer>> getPositions() {
        return positions;
    }
}