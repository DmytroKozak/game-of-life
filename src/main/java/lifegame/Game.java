package lifegame;

public class Game {

    public static void main(String[] args) {
        int rows = 25;
        int columns = 25;
        GameProcessor gameProcessor = new GameProcessor(rows, columns, Pattern.GLIDER);
        while (true) {
            gameProcessor.displayBoard();
            addDelay();
            gameProcessor.startGame();
        }
    }

    private static void addDelay() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}