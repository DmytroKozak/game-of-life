package lifegame;

public class GameProcessor {

    private final int rows;
    private final int columns;
    private final Board board;
    private final Board tempBoard;

    public GameProcessor(int rows, int columns, Pattern pattern) {
        this.rows = rows;
        this.columns = columns;
        board = new Board(rows, columns);
        tempBoard = new Board(rows, columns);
        initializeBoard(board, pattern);
    }

    public void startGame() {
        calculateNextGeneration();
        transferTempToCurrent();
    }

    private void initializeBoard(Board board, Pattern pattern) {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                board.set(r, c, CellState.DEAD);
            }
        }
        populateAccordingToPattern(board, pattern); //TODO: add a check that the size of the board allows to place a figure according to the pattern
    }

    private void calculateNextGeneration() {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                int neighborCount = countNeighbors(r, c);
                boolean alive = isAlive(r, c);
                if (alive && neighborCount < 2) {
                    tempBoard.set(r, c, CellState.DEAD);
                } else if (alive && neighborCount < 4) {
                    tempBoard.set(r, c, CellState.ALIVE);
                } else if (!alive && neighborCount == 3) {
                    tempBoard.set(r, c, CellState.ALIVE);
                } else {
                    tempBoard.set(r, c, CellState.DEAD);
                }
            }
        }
    }

    private void transferTempToCurrent() {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                board.set(r, c, tempBoard.get(r, c));
            }
        }
    }

    private int countNeighbors(int row, int column) {
        int count = 0;
        for (int r = row - 1; r <= row + 1; r++) {
            for (int c = column - 1; c <= column + 1; c++) {
                if (r >= 0 && r < rows && c >= 0 && c < columns
                        && !(r == row && c == column)
                        && isAlive(r, c)) {
                    count++;
                }
            }
        }
        return count;
    }

    private boolean isAlive(int r, int c) {
        return board.get(r, c) == CellState.ALIVE;
    }

    private void populateAccordingToPattern(Board board, Pattern pattern) {
            pattern.getPositions().forEach(pair -> board.set(pair.getLeft(), pair.getRight(), CellState.ALIVE));
    }

    // temporary method to display the game state to the console
    public void displayBoard() {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                if (isAlive(r, c)) {
                    System.out.print("[] ");
                } else
                    System.out.print("__ ");
            }
            System.out.println();
        }
    }
}